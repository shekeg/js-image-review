let drawArea = document.createElement('canvas');
const ctx = drawArea.getContext('2d');
let drawing = false;

function drawingMask() {
  drawArea = document.querySelector('canvas') || drawArea
  menu.dataset.state = 'selected';
  drawMenu.dataset.state = 'selected';
  drawArea.classList.add('on-top-plus');
  let drawColor = drawMenu.nextElementSibling
    .querySelector('[checked]')
    .getAttribute('value');

  Array.from(drawMenu.nextElementSibling.children).forEach(item => {
    item.addEventListener('change', () => {
      drawArea.classList.add('on-top-plus');
      Array.from(drawMenu.nextElementSibling.children).forEach(item => {
        item.removeAttribute('checked');
      });
      item.setAttribute('checked', '');
      drawColor = item.getAttribute('value');
    });
  });

  function drawCreate(x, y) {
    ctx.strokeStyle = drawColor;
    ctx.fillStyle = drawColor;
    ctx.lineCap = 'round';
    ctx.lineJoin = 'round';
    ctx.lineWidth = 4;
    ctx.lineTo(x, y);
    ctx.stroke();
  }

  drawArea.addEventListener('mousedown', event => {
    event.preventDefault();
    if (drawMenu.dataset.state === 'selected') {
      drawing = true;
      ctx.beginPath();
      ctx.moveTo(event.offsetX, event.offsetY);
    }
  });

  drawArea.addEventListener('mouseup', event => {
    event.stopPropagation();
    drawing = false;
    if (drawMenu.dataset.state === 'selected') {
      sendMask();
    }
  });

  drawArea.addEventListener('mouseleave', () => {
    drawing = false;
  });

  drawArea.addEventListener('mousemove', event => {
    event.preventDefault();
    if (drawing) {
      drawCreate(event.offsetX, event.offsetY);
    }
  });
}