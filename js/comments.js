const newCommentFormTemplate = {
  tag: 'form',
  block: 'comments',
  elem: 'form',
  content: [{
      tag: 'span',
      elem: 'marker'
    },
    {
      tag: 'input',
      elem: 'marker-checkbox',
      attrs: {
        type: 'checkbox',
      }
    },
    {
      tag: 'div',
      elem: 'body',
      cls: 'active',
      content: [{
          block: 'comment',
          content: {
            block: 'loader',
            attrs: {
              style: 'display: none'
            },
            content: [{
                tag: 'span'
              },
              {
                tag: 'span'
              },
              {
                tag: 'span'
              },
              {
                tag: 'span'
              },
              {
                tag: 'span'
              }
            ]
          }
        },
        {
          tag: 'textarea',
          block: 'comments',
          elem: 'input',
          attrs: {
            type: 'text',
            placeholder: 'Напишите ваш комментарий...'
          }
        },
        {
          tag: 'input',
          block: 'comments',
          elem: 'close',
          attrs: {
            type: 'button',
            value: 'Закрыть'
          }
        },
        {
          tag: 'input',
          block: 'comments',
          elem: 'submit',
          attrs: {
            type: 'submit',
            value: 'Отправить'
          }
        }
      ]
    }
  ]
};

const newCommentTemplate = {
  block: 'comment',
  content: [{
      tag: 'p',
      elem: 'time',
    },
    {
      tag: 'p',
      elem: 'message',
    }
  ]
};

const formContainer = createElement({
  block: 'form-container'
});

function toCommentsMenu() {
  Array.from(menu.children).forEach(item => {
    item.dataset.state = '';
  });
  menu.dataset.state = 'selected';
  commentsMenu.dataset.state = 'selected';
  if (commentsToggle[0].hasAttribute('checked')) {
    showComments();
  }
  //скрыть / показать комментарии
  commentsToggle[0].addEventListener('change', event => {
    commentsToggle[1].removeAttribute('checked');
    commentsToggle[0].setAttribute('checked', '');
    showComments();
  });

  commentsToggle[1].addEventListener('change', event => {
    commentsToggle[0].removeAttribute('checked');
    commentsToggle[1].setAttribute('checked', '');
    hideComments();
  });
  formContainer.classList.add('on-top');
}

function showComments() {
  document.querySelectorAll('.comments__form').forEach(item => {
    item.style.display = 'block';
  });
  formContainer.classList.add('on-top');
}

function hideComments() {
  document.querySelectorAll('.comments__form').forEach(item => {
    item.style.display = 'none';
  });
}

function closeComments() {
  document.querySelectorAll('.comments__body').forEach(item => {
    if (Array.from(item.children).length === 4) {
      item.parentNode.parentNode.removeChild(item.parentNode);
    }
  });
  document.querySelectorAll('.comments__body.active').forEach(
    item => {
      item.classList.remove('active');
    }
  );
}

function loadComments(comments) {
  const activeComment = app.querySelector('.comments__body.active');

  Object.keys(comments).forEach(key => {
    const commentId = comments[key].left + '&' + comments[key].top;
    let idCount = 0;
    const oldComments = app.querySelectorAll('.comments__form');

    oldComments.forEach(item => {
      if (item.dataset.commentId === commentId) {
        idCount++;
      }
    });
    if (idCount === 0) {
      createNewCommentBlock(comments[key].left, comments[key].top);
    }
    createNewCommentText(
      commentId,
      comments[key].timestamp,
      comments[key].message
    );
  });
  closeComments();
  if (activeComment) {
    activeComment.classList.add('active');
  }
  if (commentsToggle[0].hasAttribute('checked')) {
    showComments();
  } else {
    hideComments();
  }
}

function createNewCommentBlock(x, y) {
  const commentForm = createElement(newCommentFormTemplate);
  const commentText = commentForm.querySelector('.comments__input');

  formContainer.appendChild(commentForm);
  commentForm.setAttribute('style', `top:${y}px; left:${x}px`);
  commentForm.dataset.commentId = x + '&' + y;
  commentText.focus();
  // события для созданной формы комментария
  commentForm.children[1].addEventListener('click', event => {
    if (commentForm.lastElementChild.classList.contains('active')) {
      closeComments();
    } else {
      closeComments();
      commentForm.lastElementChild.classList.add('active');
      commentText.focus();
    }
  });

  commentForm
    .querySelector('.comments__close')
    .addEventListener('click', closeComments);

  commentForm
    .querySelector('.comments__submit')
    .addEventListener('click', event => {
      event.preventDefault();
      const commentText = commentForm.querySelector('.comments__input');
      commentText.focus();
      if (commentText.value) {
        commentForm.querySelector('.loader').style.display = 'block';
        const commentData =
          'message=' +
          encodeURIComponent(commentText.value) +
          '&left=' +
          encodeURIComponent(x) +
          '&top=' +
          encodeURIComponent(y);
        const start = wsConnection.url.indexOf('pic/');
        const pic = wsConnection.url.substring(start + 4);
        commentText.value = '';
        fetch(`https://neto-api.herokuapp.com/pic/${pic}/comments`, {
          body: commentData,
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        }).catch(showError);
      }
    });
}

function createNewCommentText(commentId, time, text) {
  app.querySelectorAll('.comments__form').forEach(item => {
    if (item.dataset.commentId === commentId) {
      const commentText = createElement(newCommentTemplate);
      const commentTime = new Date(time);
      const lastComment = item.querySelector('.loader').parentElement;
      const commentsArea = item.querySelector('.comments__body');

      commentText.firstElementChild.innerText = commentTime.toLocaleString(
        'ru-RU'
      );
      commentText.lastElementChild.innerText = text;
      commentsArea.insertBefore(commentText, lastComment);
      item.querySelector('.loader').style.display = 'none';
    }
  });
}

//создание комментариев
formContainer.addEventListener('click', event => {
  if (
    event.target === event.currentTarget &&
    commentsMenu.dataset.state === 'selected' &&
    commentsToggle[0].hasAttribute('checked')
  ) {
    closeComments();
    const formX =
      event.pageX - (drawArea.offsetLeft - drawArea.offsetWidth / 2);
    const formY =
      event.pageY - (drawArea.offsetTop - drawArea.offsetHeight / 2);
    createNewCommentBlock(formX, formY);
  }
});