'use strict';

const app = document.querySelector('.app');
const mainPath = window.location.host + window.location.pathname;

// ~~~~~~~~~~ functions ~~~~~~~~~~ //

function startingApp() {
  menu.dataset.state = 'initial';
  burgerMenu.style.display = 'none';
  menuPositionCalc(menu);
}

//загрузка страницы
document.addEventListener('DOMContentLoaded', () => {
  //создание контейнера для комментариев
  app.appendChild(formContainer);
  //удаление примера чата комментария
  document
    .querySelector('.comments__form')
    .parentNode.removeChild(document.querySelector('.comments__form'));
  //установка позиции меню
  menuPositionCalc(menu);
  //удаление ссылки на фон
  background.removeAttribute('src');
  //вставка инпута в меню
  inputImage.innerText = '';
  inputImage.appendChild(createElement(newInputImageTemplate));

  //загрузка нового фона по событию 'Загрузки'
  const inputNewImage = document.querySelector('.menu__item.new input');

  inputNewImage.addEventListener('change', event => {
    event.preventDefault();
    const [file] = event.currentTarget.files;
    loadBackground(file);
  });

  //Проверка ссылки, попытка загрузки с сервера
  const shareUrl = getUrlId() ? getUrlId().id : sessionStorage.backgroundId;
  if (shareUrl) {
    fetch(`https://neto-api.herokuapp.com/pic/${shareUrl}`)
      .then(data => {
        return data.json();
      })
      .then(data => {
        document
          .querySelector('.menu__url')
          .setAttribute('value', `${mainPath}?${data.id}`);
        wsConnect(data.id);
        if (getUrlId()) {
          toCommentsMenu();
        } else {
          shareImage();
        }
      })
      .catch(data => {
        showError('Изображение по ссылке не найдено');
        startingApp();
      });
  } else {
    startingApp();
  }
});

//загрузка файла переносом
app.addEventListener('dragover', event => event.preventDefault());

app.addEventListener('drop', event => {
  event.preventDefault();
  if (background.src) {
    showError(
      'Чтобы загрузить новое изображение, пожалуйста воспользуйтесь пунктом "Загрузить новое" в меню'
    );
    return;
  }
  const [file] = event.dataTransfer.files;
  loadBackground(file);
});

document.addEventListener('mousemove', event => {
  if (dragMenu) {
    event.preventDefault();
    if (event.pageX - dragMenu.offsetWidth / 2 < 0) {
      menu.style.setProperty('--menu-left', 0 + 'px');
      localStorage.menuLeft = 0;
    } else if (
      event.pageX + menu.offsetWidth >
      document.documentElement.clientWidth - dragMenu.offsetWidth / 2
    ) {
      menu.style.setProperty(
        '--menu-left',
        document.documentElement.clientWidth - menu.offsetWidth - 1 + 'px'
      );
      localStorage.menuLeft =
        document.documentElement.clientWidth - menu.offsetWidth - 1;
    } else {
      menu.style.setProperty(
        '--menu-left',
        event.pageX - dragMenu.offsetWidth / 2 + 'px'
      );
      localStorage.menuLeft = event.pageX - dragMenu.offsetWidth / 2;
    }
    if (event.pageY - dragMenu.offsetHeight / 2 < 0) {
      menu.style.setProperty('--menu-top', 0 + 'px');
      localStorage.menuTop = 0;
    } else if (
      event.pageY + menu.offsetHeight >
      document.documentElement.clientHeight - dragMenu.offsetWidth / 2
    ) {
      menu.style.setProperty(
        '--menu-top',
        document.documentElement.clientHeight - menu.offsetHeight + 'px'
      );
      localStorage.menuTop =
        document.documentElement.clientHeight - menu.offsetHeight;
    } else {
      menu.style.setProperty(
        '--menu-top',
        event.pageY - dragMenu.offsetHeight / 2 + 'px'
      );
      localStorage.menuTop = event.pageY - dragMenu.offsetHeight / 2;
    }
  }
});

document.addEventListener('mouseup', event => {
  if (dragMenu) {
    dragMenu = null;
  }
});

function showError(text) {
  background.style.display = 'none';
  drawArea.style.display = 'none';
  mask.style.display = 'none';
  document.querySelectorAll('.comments__form').forEach(commentForm => {
    commentForm.style.display = 'none';
  })
  document.querySelector('.image-loader').style.display = 'none';
  document.querySelector('.error__message').innerText = text;
  document.querySelector('.error').style.display = 'block';
  app.addEventListener('click', hideError);
}

function hideError() {
  app.removeEventListener('click', hideError);
  menu.style.display = '';
  background.style.display = 'block';
  document.querySelector('.error').style.display = 'none';
  drawArea.style.display = 'block';
  mask.style.display = 'block';
  document.querySelectorAll('.comments__form').forEach(commentForm => {
    commentForm.style.display = 'block';
  })
}