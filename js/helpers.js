function checkWidth(block, posLeft) {
  if (block.offsetHeight < menuHeight) return;
  posLeft--;
  block.style.setProperty('--menu-left', posLeft + 'px');
  localStorage.menuLeft = posLeft;
}

function createElement(node, parent = null) {
  if (typeof node === 'string') {
    return document.createTextNode(node.toString());
  }
  if (Array.isArray(node)) {
    return node.reduce((f, elem) => {
      f.appendChild(createElement(elem, parent));
      return f;
    }, document.createDocumentFragment());
  }
  const tag = node.tag ? node.tag : 'div';

  const element = document.createElement(tag);

  if (node.block && !node.elem) {
    element.classList.add(node.block);
  }

  if (!node.block && node.elem) {
    element.classList.add(`${parent.block}__${node.elem}`);
  }

  if (node.block && node.elem) {
    element.classList.add(`${node.block}__${node.elem}`);
  }

  if (node.cls) {
    node.cls.split(' ').forEach(cls => 
      element.classList.add(cls)
    )
  }

  if (node.attrs) {
    Object.keys(node.attrs).forEach(key =>
      element.setAttribute(key, node.attrs[key])
    );
  }

  if (node.content) {
    element.appendChild(createElement(node.content, node));
  }

  return element
}

function getUrlId() {
  const startId = window.location.href.indexOf('?');
  const backgroundUrl = {};
  backgroundUrl.id = window.location.href.substring(startId + 1);
  if (startId >= 0 && backgroundUrl.id.length > 1) {
    return backgroundUrl;
  } else {
    return false;
  }
}