let wsConnection;

function wsConnect(id) {
  wsConnection = new WebSocket(`wss://neto-api.herokuapp.com/pic/${id}`);
  wsConnection.addEventListener('message', event => {
    const wsData = JSON.parse(event.data);
    console.log(wsData);
    
    if (wsData.event === 'mask') {
      updateMask(wsData.url);
    }
    if (wsData.event === 'error') {
      console.log(wsData);
    }
    if (wsData.event === 'comment') {
      loadComments({
        comment: wsData.comment
      });
    }
    if (wsData.event === 'pic') {
      sessionStorage.backgroundId = wsData.pic.id;
      //удаление примера чата комментария
      if (document.querySelector('.comments__form')) {
        document
          .querySelector('.comments__form')
          .parentNode.removeChild(document.querySelector('.comments__form'));
      }
      background.src = wsData.pic.url;
      background.addEventListener('load', () => {
        //добавляем размеры и позицию канвасу
        drawArea.width = background.offsetWidth;
        drawArea.height = background.offsetHeight;
        drawArea.classList.add('draw-area');
        //позиционирование контейнера форм
        formContainer.style.width = background.offsetWidth + 'px';
        formContainer.style.height = background.offsetHeight + 'px';
        //скрытие прелоадера
        document.querySelector('.image-loader').style.display = 'none';
        menu.style.display = '';
        app.insertBefore(drawArea, background);
        //проверка позиции меню
        if (menu.offsetHeight > menuHeight) {
          checkWidth(menu, menu.offsetLeft);
        }
        //добавление маски
        mask.classList.add('mask-image');
        app.insertBefore(mask, background);
      });
      if (wsData.pic.mask !== undefined) {
        updateMask(wsData.pic.mask);
      }
      if (wsData.pic.comments !== undefined) {
        loadComments(wsData.pic.comments);
      }
    }
  });
}