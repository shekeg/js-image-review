const background = document.querySelector('.current-image');

function loadBackground(file) {
  const {
    type
  } = file;
  if (
    type !== 'image/png' &&
    type !== 'image/jpeg' &&
    type !== 'image/pƒjpeg'
  ) {
    showError(
      'Неверный формат файла. Пожалуйста, выберите изображение в формате .jpg или .png.'
    );

    return;
  }
  background.removeAttribute('src');
  mask.removeAttribute('src');
  mask.style.display = '';
  burgerMenu.style.display = '';
  createBackground(file);
}

function createBackground(img) {
  const imageForSend = new FormData();

  imageForSend.append('title', 'This is just background');
  imageForSend.append('image', img);
  hideComments();
  document.querySelector('.image-loader').style.display = 'block';
  fetch('https://neto-api.herokuapp.com/pic', {
      body: imageForSend,
      method: 'POST'
    })
    .then(data => {
      return data.json();
    })
    .then(data => {
      wsConnect(data.id);
      shareImage(data.id);
    })
    .catch(showError);
}