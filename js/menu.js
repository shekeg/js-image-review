const menu = document.querySelector('.menu');
const inputImage = document.querySelector('.menu__item.new');
const shareMenu = document.querySelector('.share');
const burgerMenu = document.querySelector('.burger');
const commentsMenu = document.querySelector('.comments');
const commentsToggle = document.querySelectorAll('.menu__toggle');
const drawMenu = document.querySelector('.draw');
let dragMenu = null;
const menuHeight = menu.offsetHeight + 5;

const newInputImageTemplate = {
  tag: 'label',
  block: 'menu',
  elem: 'item',
  content: [{
      tag: 'i',
      elem: 'icon',
      cls: 'new-icon'
    },
    {
      tag: 'span',
      elem: 'item-title',
      content: [
        'Загрузить',
        {
          tag: 'br'
        },
        'новое'
      ]
    },
    {
      tag: 'input',
      attrs: {
        type: 'file',
        style: 'display: none;',
        accept: 'image/jpeg, image/pjpeg, image/png'
      }
    }
  ]
};

//перетаскивание меню
menu.firstElementChild.addEventListener('mousedown', event => {
  dragMenu = event.currentTarget;
});

menu.addEventListener('click', event => {
  formContainer.classList.remove('on-top');
  drawArea.classList.remove('on-top-plus');
  if (event.target === burgerMenu || event.target.parentNode === burgerMenu) {
    Array.from(menu.children).forEach(item => {
      item.dataset.state = '';
    });
    menu.dataset.state = 'default';
  }

  if (event.target === shareMenu || event.target.parentNode === shareMenu) {
    Array.from(menu.children).forEach(item => {
      item.dataset.state = '';
    });
    shareImage();
  }

  if (event.target === drawMenu || event.target.parentNode === drawMenu) {
    Array.from(menu.children).forEach(item => {
      item.dataset.state = '';
    });
    drawingMask();
  }

  if (
    event.target === commentsMenu ||
    event.target.parentNode === commentsMenu
  ) {
    toCommentsMenu();
  }
  menuPositionCalc(menu);
});

function menuPositionCalc(block) {
  const windowHeight = document.documentElement.clientHeight;
  const windowWidth = document.documentElement.clientWidth;
  const blockHeight = block.offsetHeight;
  const blockWidth = block.offsetWidth;
  const posTop = localStorage.menuTop ?
    localStorage.menuTop :
    (windowHeight - blockHeight) / 2;
  const posLeft = localStorage.menuLeft ?
    localStorage.menuLeft :
    (windowWidth - blockWidth) / 2;

  block.style.setProperty('--menu-top', posTop + 'px');
  //защита от переламывания меню
  checkWidth(menu, posLeft);
}

//копирование ссылки
document.querySelector('.menu_copy').addEventListener('click', event => {
  menu.querySelector('.menu__url').select();
  try {
    const successful = document.execCommand('copy');
    const msg = successful ? 'успешно ' : 'не';
    console.log(`URL ${msg} скопирован`);
  } catch (err) {
    console.log('Ошибка копирования');
  }
  window.getSelection().removeAllRanges();
});