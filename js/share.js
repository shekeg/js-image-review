function shareImage(id) {
  menu.dataset.state = 'selected';
  shareMenu.dataset.state = 'selected';
  if (id) {
    document
      .querySelector('.menu__url')
      .setAttribute('value', `${mainPath}?${id}`);
  }
  menuPositionCalc(menu);
}